#!/bin/bash

# --- Récupération des options --- #

OPTION_DEMON=$(getopt -o h,d -l help,demon -- "$@") #option démoniaque -d entrée dans $@

eval set -- "$OPTION_DEMON"

# --- Non démon --- #

if [ $1 != "-d" ] && [ $1 != "--demon" ] && [ $1 != "-h" ] && [ $1 != "--help" ]
  aleatoire=$(( $RANDOM % 666))
  if [ $aleatoire -lt 1000 ] 
      then 
        answer=$(($aleatoire*666/$aleatoire))
        echo $answer #dit une fois 666, de facon non démoniaque
  fi

# --- Démon --- #

elif [ $1 == "-d" ] || [ $1 == "--demon" ]
    then
        aleatoire=$(( $RANDOM % 666))
        while [ $aleatoire -lt 1000 ]
        do 
          echo $(($aleatoire*666/$aleatoire)) #répète infiniment 666 en utilisant de l'aléatoire
        done;

# --- Aide --- #

else
    echo "--- Options disponibles ---"
    echo "-h ou --help affiche cette page d'aide"
    echo "-d ou --demon lance une boucle infernale de nombres aléatoires tous égaux à 666"
    echo "pas d'option équivaut à générer une seule fois un nombre aléatoire égal à 666"
fi

# --- Nettoyage --- #

unset OPTION_DEMON #nettoie la variable inutile
